package com.example.jana.beerpoint.database;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Jana on 6. 11. 2015.
 */

public class Pub extends SugarRecord<Pub>{
    private String name;
    private String address;
    private String phone;
    private String uri;
    private Double latitude;
    private Double longitude;
    @Ignore
    private List<PubHasBeer> beerOffer;
    private boolean isBrewery;
    private boolean isNonstop;
    private float atmosphere;
    private float service;
    private float availability;
    private float quality;
    private float price;
    private Beer beer;

    public Pub(){
    }

    public Pub(String name, String address, String phone, String uri, Double latitude,
               Double longitude, boolean isBrewery, boolean isNonstop, float atmosphere,
               float service, float availability, float quality, float price){
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.uri = uri;
        this.latitude = latitude; 
        this.longitude = longitude;
        this.isBrewery = isBrewery;
        this.isNonstop = isNonstop;
        this.atmosphere = atmosphere;
        this.service = service;
        this.availability = availability;
        this.quality = quality;
        this.price = price;
    }

    public Pub(String name){
        this.setName(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return uri;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setBeerOffer(List<PubHasBeer> beerOffer) {
        this.beerOffer = beerOffer;
    }

    public List<Beer> getBeerOffer(){
        ArrayList<PubHasBeer> tmpPubList = (ArrayList<PubHasBeer>) PubHasBeer.find(PubHasBeer.class,
                "pub = ?", this.getId().toString());
        LinkedList<Beer> tmpBeerList = new LinkedList<>();
        for (PubHasBeer phb : tmpPubList) {
            tmpBeerList.add(phb.getBeer());

        }
        return tmpBeerList;
    }

    /*public List<User> getUserRating(){
        ArrayList<UserRatePub> tmpUserRating = (ArrayList<UserRatePub>) UserRatePub.find(UserRatePub.class,
                "pub = ?", this.getId().toString());
        LinkedList<User> tmpBeerList = new LinkedList<>();
        for (UserRatePub urp : tmpUserRating) {
            tmpBeerList.add(urp.;

        }
        return tmpBeerList;
    }*/

    public void setIsBrewery(boolean isBrewery) {
        this.isBrewery = isBrewery;
    }

    public  boolean getIsBrevery() {
        return  isBrewery;
    }

    public  void setIsNonstop(boolean isNonstop) {
        this.isNonstop = isNonstop;
    }

    public boolean getIsNonstop() {
        return isNonstop;
    }

    public void setAtmosphere(float atmosphere) {
        this.atmosphere = atmosphere;
    }

    public float getAtmosphere() {
        return atmosphere;
    }

    public void setService(float service) {
        this.service = service;
    }

    public float getService() {
        return service;
    }

    public void setAvailability(float availability) {
        this.availability = availability;
    }

    public float getAvailability() {
        return availability;
    }

    public void setQuality(float quality) {
        this.quality = quality;
    }

    public float getQuality() {
        return quality;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Pub [id= " + id + ", name= " + name + ", address= " + address
                + ", phone= " + phone + ", uri= " + uri
                + ", isBrewery= " + isBrewery + ", isNonstop= " + isNonstop
                + ", atmosphere= " + atmosphere + ", service= " + service
                + ", availability= " + availability + ", quality= " + quality
                + ", price= " + price + "]";
    }

}
