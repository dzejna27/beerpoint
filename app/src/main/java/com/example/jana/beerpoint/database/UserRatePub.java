package com.example.jana.beerpoint.database;

import com.orm.SugarRecord;

/**
 * Created by Jana on 28. 11. 2015.
 */
public class UserRatePub extends SugarRecord<UserRatePub> {
    private User user;
    private Pub pub;
    private float atmosphere;
    private float service;
    private float availability;
    private float quality;
    private float pubPrice;

    public UserRatePub() {

    }

    public UserRatePub(User user, Pub pub, float atmosphere, float service, float availability, float quality, float pubPrice) {
        this.setUser(user);
        this.setPub(pub);
        this.setAtmosphere(atmosphere);
        this.setService(service);
        this.setAvailability(availability);
        this.setQuality(quality);
        this.setPubPrice(pubPrice);
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Pub getPub() {
        return pub;
    }

    public void setPub(Pub pub) {
        this.pub = pub;
    }

    public float getAtmosphere() {
        return atmosphere;
    }

    public void setAtmosphere(float atmosphere) {
        this.atmosphere = atmosphere;
    }

    public float getService() {
        return service;
    }

    public void setService(float service) {
        this.service = service;
    }

    public float getAvailability() {
        return availability;
    }

    public void setAvailability(float availability) {
        this.availability = availability;
    }

    public float getQuality() {
        return quality;
    }

    public void setQuality(float quality) {
        this.quality = quality;
    }

    public float getPubPrice() {
        return pubPrice;
    }

    public void setPubPrice(float pubPrice) {
        this.pubPrice = pubPrice;
    }

    @Override
    public String toString() {
        return "User rate beer [id= " + id + ", user= " + getUser() + ", pub= " + getPub() +
                ",  atmosphere= " + atmosphere + ",  service= " + service +
                ",  availability= " + availability + ",  quality= " + quality +
                ",  pubPrice= " + pubPrice + "]";
    }
}
