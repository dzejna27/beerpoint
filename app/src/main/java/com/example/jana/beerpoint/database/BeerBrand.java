package com.example.jana.beerpoint.database;

import com.example.jana.beerpoint.database.Beer;
import com.orm.SugarRecord;

/**
 * Created by Jana on 17. 11. 2015.
 */
public class BeerBrand extends SugarRecord<Beer> {
    private String brand;

    public BeerBrand(){
    }

    public BeerBrand(String brand) {
        this.setBeerBrand(brand);
    }

    public void setBeerBrand(String brand) {
        this.brand = brand;
    }

    public String getBeerBrand() {
        return brand;
    }

    @Override
    public String toString() {
        return "Beer brand [id= " + id + " brand= " + brand + " ]";
    }

//    @Override
//    public String toString() {
//        return brand;
//    }
}
