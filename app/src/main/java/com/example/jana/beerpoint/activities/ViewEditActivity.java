package com.example.jana.beerpoint.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jana.beerpoint.R;
import com.example.jana.beerpoint.database.Beer;
import com.example.jana.beerpoint.database.BeerBrand;
import com.example.jana.beerpoint.database.BeerStyle;
import com.example.jana.beerpoint.database.Country;
import com.example.jana.beerpoint.database.Pub;
import com.example.jana.beerpoint.database.PubHasBeer;

import java.util.LinkedList;
import java.util.List;

public class ViewEditActivity extends AppCompatActivity {

    private AutoCompleteTextView mBeerName;
    private AutoCompleteTextView mBrand;
    private AutoCompleteTextView mStyle;
    private EditText mAlc;
    private AutoCompleteTextView mCountry;
    private RatingBar mTaste;
    private RatingBar mBeerPrice;

    private AutoCompleteTextView mName;
    private EditText mAddress;
    private EditText mPhone;
    private EditText mUri;
    private MultiAutoCompleteTextView mBeerOffer;;
    private RatingBar mAvailability;
    private RatingBar mAtmosphere;
    private RatingBar mService;
    private RatingBar mQuality;
    private RatingBar mPubPrice;

    private List<Beer> draughtedBeer = new LinkedList<>();
    PubHasBeer bp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle parameters = getIntent().getExtras();
        if (parameters != null) {
            if (parameters.containsKey("beer")) {
                setContentView(parameters.getInt("beer"));

                mBeerName = (AutoCompleteTextView) findViewById(R.id.autoCompleteBeerName2);
                List<Beer> beerList = Beer.listAll(Beer.class);
                String[] beerListString = new String[beerList.size()];
                System.out.println(beerList);
                for (int i = 0; i < beerList.size(); i++)
                    beerListString[i] = beerList.get(i).getBeerName();
                ArrayAdapter<String> aBeerOffer = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, beerListString);
                mBeerName.setAdapter(aBeerOffer);

                mBrand = (AutoCompleteTextView) findViewById(R.id.autoCompleteBeerBrand2);
                mStyle = (AutoCompleteTextView) findViewById(R.id.autoCompleteBeerStyle2);
                String[] brands = getResources().getStringArray(R.array.beer_brands);
                ArrayAdapter<String> aBrand = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, brands);
                mBrand.setAdapter(aBrand);

                mStyle = (AutoCompleteTextView) findViewById(R.id.autoCompleteBeerStyle2);
                String[] styles = getResources().getStringArray(R.array.beer_styles);
                ArrayAdapter<String> aStyle = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, styles);
                mStyle.setAdapter(aStyle);


                mAlc = (EditText) findViewById(R.id.textBeerAlcView2);
                mCountry = (AutoCompleteTextView) findViewById(R.id.autoCompleteCountry2);
                mTaste = (RatingBar) findViewById(R.id.ratingBarTaste2);
                mBeerPrice = (RatingBar) findViewById(R.id.ratingBarBeerPrice2);
            }
            if (parameters.containsKey("pub")) {
                setContentView(parameters.getInt("pub"));
                mName = (AutoCompleteTextView) findViewById(R.id.AutoCompletePubName2);
                List<Pub> pubList = Pub.listAll(Pub.class);
                String[] pubListString = new String[pubList.size()];
                System.out.println(pubList);
                for (int i = 0; i < pubList.size(); i++)
                    pubListString[i] = pubList.get(i).getName();
                ArrayAdapter<String> aPubOffer = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, pubListString);
                mName.setAdapter(aPubOffer);

                mAddress = (EditText) findViewById(R.id.editAddress2);
                mPhone = (EditText) findViewById(R.id.editPhone2);
                mUri = (EditText) findViewById(R.id.editUri2);
                mBeerOffer = (MultiAutoCompleteTextView)
                        findViewById(R.id.multiAutoCompleteBeerOffer2);
                mAtmosphere = (RatingBar) findViewById(R.id.ratingBarAtmosphere2);
                mService = (RatingBar) findViewById(R.id.ratingBarService2);
                mAvailability = (RatingBar) findViewById(R.id.ratingBarAvailability2);
                mQuality = (RatingBar) findViewById(R.id.ratingBarQuality2);
                mPubPrice = (RatingBar) findViewById(R.id.ratingBarPubPrice2);
            }
        }
    }

    public void onSearchBeer(View view) {
        List<Beer> searchedBeer = Beer.find(Beer.class, "beer_name = ? ", mBeerName.getText().toString());
        Beer beer = Beer.findById(Beer.class, searchedBeer.get(0).getId());
        mBrand.setText(beer.getBrand().getBeerBrand().toString());
        mStyle.setText(beer.getStyle().getBeerStyle().toString());
        mAlc.setText(Float.toString(beer.getAlc()));
        mCountry.setText(beer.getCountry().toString());
        mTaste.setRating(beer.getRatingTaste());
        mBeerPrice.setRating(beer.getRatingPrice());
    }

    public void onSaveBeer(View view) {
        List<Beer> searchedBeer = Beer.find(Beer.class, "beer_name = ? ", mBeerName.getText().toString());
        Beer beer = Beer.findById(Beer.class, searchedBeer.get(0).getId());

        final List<BeerBrand> brandTmp = BeerBrand.find(BeerBrand.class,
                "brand = ?", mBrand.getText().toString());
        final List<BeerStyle> styleTmp = BeerStyle.find(BeerStyle.class,
                "style = ?", mStyle.getText().toString());
        final List<Country> countryTmp = Country.find(Country.class,
                "country = ?", mCountry.getText().toString());

        beer.setBeerName(mBeerName.getText().toString());
        beer.setBrand(brandTmp.get(0));
        beer.setStyle(styleTmp.get(0));
        beer.setAlc(Float.parseFloat(mAlc.toString()));
        beer.setCountry(countryTmp.get(0));
        beer.save();
        Toast.makeText(getBaseContext(),
                "Beer edited", Toast.LENGTH_LONG).show();
    }

    public void onSearchPub(View view) {
        List<Pub> searchedPub = Pub.find(Pub.class, "name = ? ", mName.getText().toString());
        Pub pub = Pub.findById(Pub.class, searchedPub.get(0).getId());
        mAddress.setText(pub.getAddress().toString());
        mPhone.setText(pub.getPhone().toString());
        mUri.setText(pub.getUri().toString());

        List<Beer> draughtedBeers = pub.getBeerOffer();
        String beerListString = "";
        for (int i = 0; i < draughtedBeers.size(); i++)
        {
            beerListString += draughtedBeers.get(i).getBeerName();
            if (  (i < (draughtedBeers.size() - 1)) || (i != 0) )
                beerListString += ", ";
        }
        mBeerOffer.setText(beerListString);

        mAtmosphere.setRating(pub.getAtmosphere());
        mService.setRating(pub.getService());
        mAvailability.setRating(pub.getAvailability());
        mQuality.setRating(pub.getQuality());
        mPubPrice.setRating(pub.getPrice());
    }

    public void onSavePub(View view) {
        List<Pub> searchedPub = Pub.find(Pub.class, "name = ? ", mName.getText().toString());
        Pub pub = Pub.findById(Pub.class, searchedPub.get(0).getId());

        pub.setName(mName.getText().toString());
        pub.setPhone(mPhone.getText().toString());
        pub.setUri(mUri.getText().toString());
        pub.save();

        String[] whereArgs = mBeerOffer.getText().toString().split(", ");
        String whereClause = "";
        if(whereArgs.length > 0){
            whereClause = "beer_name = ? ";
        }
        for(int i=1 ;i<whereArgs.length; i++){
            whereClause += "OR beer_name = ? ";
        }
        draughtedBeer = Beer.find(Beer.class, whereClause, whereArgs);

        for (Beer b:draughtedBeer){
            bp = new PubHasBeer(b, pub);
            bp.save();
        };
        Toast.makeText(getBaseContext(),
                "Pub edited", Toast.LENGTH_LONG).show();
    }

    public void clkOK(View view) {
        Intent iMain = new Intent(this, MainActivity.class);
        startActivity(iMain);
        return;
    }
}
