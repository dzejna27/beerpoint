package com.example.jana.beerpoint.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.MultiAutoCompleteTextView;

import com.example.jana.beerpoint.R;
import com.example.jana.beerpoint.database.Beer;
import com.example.jana.beerpoint.database.Pub;
import com.example.jana.beerpoint.database.PubHasBeer;
import com.example.jana.beerpoint.database.User;
import com.example.jana.beerpoint.database.UserHasBeerList;

import java.util.LinkedList;
import java.util.List;

public class BPBMenuActivity extends AppCompatActivity implements View.OnClickListener{

    private MultiAutoCompleteTextView mUserBeerList;
    private List<Beer> favoriteBeer = new LinkedList<>();
    private UserHasBeerList userList;
    private List<UserHasBeerList> listTmp = new LinkedList<>();
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bpb_menu);

        Button button1 = (Button) findViewById(R.id.bMap);
        button1.setOnClickListener(this);

        Button button2 = (Button) findViewById(R.id.bAdd);
        button2.setOnClickListener(this);

        Button button3 = (Button) findViewById(R.id.bView);
        button3.setOnClickListener(this);

        Button button4 = (Button) findViewById(R.id.bRate);
        button4.setOnClickListener(this);

        Button button5 = (Button) findViewById(R.id.bBeerList);
        button5.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Bundle parameters = getIntent().getExtras();
        switch (view.getId()) {
            case R.id.bMap:
                Intent iMap = new Intent(this, MapsActivity.class);
                if (parameters != null) {
                    if (parameters.containsKey("beer"))
                        iMap.putExtra("beer", R.layout.locate_beer);
                    if (parameters.containsKey("pub"))
                        iMap.putExtra("pub", R.layout.add_pub);
                }
                startActivity(iMap);
                break;
            case R.id.bAdd:
                Intent iAdd = new Intent(this, AddActivity.class);
                if (parameters != null) {
                    if (parameters.containsKey("beer")) ;
                        iAdd.putExtra("beer", R.layout.add_beer);
                    if (parameters.containsKey("pub"))
                        iAdd.putExtra("pub", R.layout.add_pub);
                }
                startActivity(iAdd);
                break;
            case R.id.bRate:
                Intent iRate = new Intent(this, RateActivity.class);
                if (parameters != null) {
                    if (parameters.containsKey("beer"))
                        iRate.putExtra("beer", R.layout.rate_beer);
                    if (parameters.containsKey("pub"))
                        iRate.putExtra("pub", R.layout.rate_pub);
                }
                startActivity(iRate);
                break;
            case R.id.bView:
                Intent iViewEdit = new Intent(this, ViewEditActivity.class);
                if (parameters != null) {
                    if (parameters.containsKey("beer"))
                        iViewEdit.putExtra("beer", R.layout.view_edit_beer);
                    if (parameters.containsKey("pub"))
                        iViewEdit.putExtra("pub", R.layout.view_edit_pub);
                startActivity(iViewEdit);
                }
                break;
            case R.id.bBeerList:
                setContentView(R.layout.user_beer_list);

                mUserBeerList = (MultiAutoCompleteTextView)
                        findViewById(R.id.multiAutoCompleteUserBeerList);

                final List<User> userTmp = User.find(User.class, "user_name = ?", "user123");
                List<UserHasBeerList> tmp = UserHasBeerList.findWithQuery(UserHasBeerList.class,
                        "SELECT DISTINCT beer FROM user_has_beer_list where user = "
                                + userTmp.get(0).getId());
                String beerListString = "";
                for (int i = 0; i < tmp.size(); i++)
                {
                    beerListString += tmp.get(i).getBeer().getBeerName();
                    if ( (i < (tmp.size() - 1)) || (i != 0) )
                        beerListString += ", ";
                }
                mUserBeerList.setText(beerListString);
                break;

        }
    }
    public void clkUserBeerList(View view) {
        List<User> userTmp = User.find(User.class, "user_name = ?", "user123");
        String[] whereArgs = mUserBeerList.getText().toString().split(", ");
        String whereClause = "";
        if(whereArgs.length > 0){
            whereClause = "beer_name = ? ";
        }
        for(int i=1 ;i<whereArgs.length; i++){
            whereClause += "OR beer_name = ? ";
        }
        favoriteBeer = Beer.find(Beer.class, whereClause, whereArgs);

        for (Beer b:favoriteBeer) {
            userList = new UserHasBeerList(user, b);
            userList.save();
        }
        Intent iMenu = new Intent(this, MainActivity.class);
        //iMenu.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(iMenu);
    }

}
