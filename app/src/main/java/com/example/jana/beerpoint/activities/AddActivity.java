package com.example.jana.beerpoint.activities;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jana.beerpoint.database.Beer;
import com.example.jana.beerpoint.database.BeerBrand;
import com.example.jana.beerpoint.database.Country;
import com.example.jana.beerpoint.database.PubHasBeer;
import com.example.jana.beerpoint.database.BeerStyle;
import com.example.jana.beerpoint.database.Pub;
import com.example.jana.beerpoint.R;
import com.example.jana.beerpoint.database.PubHasBeer;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.LinkedList;
import java.util.List;


public class AddActivity extends ActionBarActivity {
    private static final int PLACE_PICKER_REQUEST = 1;

    private EditText mBeerName;
    private AutoCompleteTextView mBrand;
    private AutoCompleteTextView mStyle;
    private EditText mAlc;
    private AutoCompleteTextView mCountry;

    private EditText mName;
    private EditText mAddress;
    private EditText mPhone;
    private EditText mUri;
    private MultiAutoCompleteTextView mBeerOffer;
    private CheckBox mIsBrewery;
    private CheckBox mIsNonstop;


    private Pub pub;
    private Beer beer;
    private BeerStyle style;
    private BeerBrand brand;
    private List<Beer> draughtedBeer = new LinkedList<>();
    PubHasBeer bp;

    private String name;
    private CharSequence address;
    private Place place;
    private CharSequence phone;
    private String uri;
    private MainActivity isBeerSession;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle parameters = getIntent().getExtras();
        if (parameters != null) {
            if (parameters.containsKey("beer")) {
                setContentView(parameters.getInt("beer"));
                mBeerName = (EditText) findViewById(R.id.editBeerName);

                mBrand = (AutoCompleteTextView) findViewById(R.id.autoCompleteBeerBrand);
                /*List<BeerBrand> queryBrand = BeerBrand.findWithQuery(BeerBrand.class, "SELECT brand from Beer_Brand");
                ArrayAdapter<BeerBrand> aBrand = new ArrayAdapter<>(this,
                        android.R.layout.simple_list_item_1, queryBrand);*/
                String[] brands = getResources().getStringArray(R.array.beer_brands);
                ArrayAdapter<String> aBrand = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, brands);
                mBrand.setAdapter(aBrand);

                mStyle = (AutoCompleteTextView) findViewById(R.id.autoCompleteBeerStyle);
                /*List<BeerStyle> queryStyle = BeerStyle.findWithQuery(BeerStyle.class, "SELECT style from Beer_Style");
                ArrayAdapter<BeerStyle> aStyle = new ArrayAdapter<>(this,
                        android.R.layout.simple_list_item_1, queryStyle);*/
                String[] styles = getResources().getStringArray(R.array.beer_styles);
                ArrayAdapter<String> aStyle = new ArrayAdapter<String>(this,
                                                android.R.layout.simple_list_item_1, styles);
                mStyle.setAdapter(aStyle);

                mAlc = (EditText) findViewById(R.id.editBeerAlc);

                mCountry = (AutoCompleteTextView) findViewById(R.id.autoCompleteCountry);
                String[] countries = getResources().getStringArray(R.array.countries_array);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, countries);
                mCountry.setAdapter(adapter);
            }

            if (parameters.containsKey("pub")) {
               setContentView(parameters.getInt("pub"));
                mName = (EditText) findViewById(R.id.editName2);
                mAddress = (EditText) findViewById(R.id.editAddress);
                mPhone = (EditText) findViewById(R.id.editPhone);
                mUri = (EditText) findViewById(R.id.editUri);
                mBeerOffer = (MultiAutoCompleteTextView)
                        findViewById(R.id.multiAutoCompleteBeerOffer);
                mIsBrewery = (CheckBox)findViewById(R.id.checkBoxIsBrewery);
                mIsNonstop = (CheckBox)findViewById(R.id.checkBoxIsNonstop);



                Toast.makeText(getBaseContext(),
                        "Wait...launching Google Places", Toast.LENGTH_LONG).show();

                try {
                    PlacePicker.IntentBuilder intentBuilder =
                            new PlacePicker.IntentBuilder();
                    //intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
                    Intent intent = intentBuilder.build(getApplicationContext());
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);

                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    @Override
    protected void onActivityResult ( int requestCode,
                                      int resultCode, Intent data){

        if (requestCode == PLACE_PICKER_REQUEST
                && resultCode == Activity.RESULT_OK) {
            place = PlacePicker.getPlace(data, this);
            name = place.getName().toString();
            System.out.println("name" + name);
            address = place.getAddress();
            phone = "";
            uri = "";
            System.out.println("place: " + address);

            if (name.startsWith("(") && name.endsWith(")")) {
                name = "";
            }
            mName.setText(name);

            if (address == null) {
                address = "";
            }
            mAddress.setText(address);

            try {
                phone = place.getPhoneNumber();
                if (phone == null) {
                    phone = "";
                }
                mPhone.setText(phone);
            } catch (NullPointerException e) {
                System.err.println("NullPointerException: " + e.getMessage());
            }

            try {
                uri = "";
                uri = place.getWebsiteUri().toString();
                Uri tmp = place.getWebsiteUri();
                if (tmp.equals(null)) {
                    uri = "";
                }
                mUri.setText(uri);
            } catch (NullPointerException e) {
                System.err.println("NullPointerException: " + e.getMessage());
            }

            List<Beer> beerList = beer.listAll(Beer.class);
            String[] beerListString = new String[beerList.size()];
            System.out.println(beerList);
            for (int i = 0; i < beerList.size(); i++)
                System.out.println(i+ "beerOffer: "+ (beerListString[i] = beerList.get(i).getBeerName()));
            ArrayAdapter<String> aBeerOffer = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, beerListString);
            mBeerOffer.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
            mBeerOffer.setThreshold(1);
            mBeerOffer.setAdapter(aBeerOffer);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void clkSaveBeer(View view) {
        final List<BeerBrand> brandTmp = BeerBrand.find(BeerBrand.class,
                "brand = ?", mBrand.getText().toString());
        final List<BeerStyle> stylesTmp = BeerStyle.find(BeerStyle.class,
                "style = ?", mStyle.getText().toString());
        final List<Country> countryTmp = Country.find(Country.class,
                "country = ?", mCountry.getText().toString());

        beer = new Beer(mBeerName.getText().toString(), brandTmp.get(0),
                stylesTmp.get(0), Float.parseFloat(mAlc.getText().toString()),
                countryTmp.get(0),(float)(0.0),(float)(0.0));
        beer.save();
        System.out.println("zobrazPivo:" + beer.listAll(Beer.class));
        Toast.makeText(getBaseContext(),
                "Beer saved", Toast.LENGTH_LONG).show();
        Intent iBPBMenu = new Intent(AddActivity.this, BPBMenuActivity.class);
        iBPBMenu.putExtra("beer", R.layout.activity_bpb_menu);
        startActivity(iBPBMenu);
    }

    public void clkSavePub(View view) {
        if (mIsBrewery.isChecked() && mIsNonstop.isChecked()) {
            Toast.makeText(getBaseContext(),
                    "Brewery cannot be nonstop", Toast.LENGTH_LONG).show();
        } else {
            String[] whereArgs = mBeerOffer.getText().toString().split(", ");
                String whereClause = "";
            if(whereArgs.length > 0){
                whereClause = "beer_name = ? ";
            }
            for(int i=1 ;i<whereArgs.length; i++){
                whereClause += "OR beer_name = ? ";
            }
            draughtedBeer = Beer.find(Beer.class, whereClause, whereArgs);

            pub = new Pub(name, address.toString(), phone.toString(), uri,
                    place.getLatLng().latitude, place.getLatLng().longitude,
                    mIsBrewery.isChecked(), mIsNonstop.isChecked(),
                    (float) (0.0), (float) (0.0), (float) (0.0), (float) (0.0), (float) (0.0));
                pub.save();

            for (Beer b:draughtedBeer){
                bp = new PubHasBeer(b, pub);
                bp.save();
            };

            System.out.println("zobrazBP:" + bp.listAll(PubHasBeer.class));
            Toast.makeText(getBaseContext(),
                    "Pub/brewery saved", Toast.LENGTH_LONG).show();
            System.out.println(pub.listAll(Pub.class));
            Intent iBPBMenu = new Intent(AddActivity.this, BPBMenuActivity.class);
            iBPBMenu.putExtra("pub", R.layout.activity_bpb_menu);
            startActivity(iBPBMenu);
        }
    }

//    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        setIntent(intent);
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//
//        Intent theIntent = getIntent();
//        if (theIntent.getExtras() != null) {
//            onNewIntent(new Intent());  // <--- "clear" the intent by setting empty one
//        }
//    }

}
