package com.example.jana.beerpoint.database;

import com.orm.SugarRecord;

/**
 * Created by Jana on 17. 11. 2015.
 */
public class Country extends SugarRecord<Beer> {
    private String country;

    public Country(){
    }

    public Country(String country) {
        this.setCountry(country);
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public String toString() {
        return "Country [id= " + id + " country= " + country + " ]";
    }

}
