package com.example.jana.beerpoint.activities;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;

import com.example.jana.beerpoint.database.Beer;
import com.example.jana.beerpoint.database.PubHasBeer;
import com.example.jana.beerpoint.database.Pub;
import com.example.jana.beerpoint.R;
import com.example.jana.beerpoint.database.User;
import com.example.jana.beerpoint.database.UserHasBeerList;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.android.gms.maps.SupportMapFragment;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.content.Context;

import android.location.Address;
import android.support.v4.app.FragmentActivity;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class MapsActivity extends FragmentActivity implements GoogleApiClient.OnConnectionFailedListener {
    private static final String LOG_TAG = "PlacesAPIActivity";
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private GoogleApiClient mGoogleApiClient;

    public static final String TAG = MapsActivity.class.getSimpleName();

    Random rand = new Random();
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    //location manager
    private LocationManager locMan;

    //user marker
    private Marker userMarker;
    private Marker pubMarker;

    //instance variables for Marker icon drawable resources
    private int userIcon;

    private int beerIcon;

    private List<Pub> pubList = new LinkedList<>();

    private CheckBox mIsBrewery;
    private CheckBox mIsNonstop;
    private CheckBox mIncludeBeerList;
    private MultiAutoCompleteTextView mSearchedBeers;

    private List<Beer> favoriteBeer = new LinkedList<>();
    private UserHasBeerList userList;
    private List<PubHasBeer> pubHasBeerTmp = new LinkedList<>();
    private User user;

    private List<Marker> markers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.locate_beer);

        //get drawable IDs
        userIcon = R.drawable.yellow_point;
        beerIcon = R.drawable.beer_marker;

        mSearchedBeers = (MultiAutoCompleteTextView) findViewById(R.id.multiAutoCompleteSearchedBeers);

        mIsBrewery = (CheckBox) findViewById(R.id.checkBoxIsBrewery);
        mIsNonstop = (CheckBox) findViewById(R.id.checkBoxIsNonstop);

        mIncludeBeerList = (CheckBox) findViewById(R.id.checkBoxBeerList);
        mIncludeBeerList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIncludeBeerList.isChecked()) {
                    final List<User> userTmp = User.find(User.class, "user_name = ?", "user123");
                    List<UserHasBeerList> tmp = UserHasBeerList.findWithQuery(UserHasBeerList.class,
                            "SELECT DISTINCT beer FROM user_has_beer_list where user = "
                                    + userTmp.get(0).getId());
                    String beerListString = "";
                    for (int i = 0; i < tmp.size(); i++) {
                        beerListString += tmp.get(i).getBeer().getBeerName();
                        if ((i < (tmp.size() - 1)) || (i != 0))
                            beerListString += ", ";
                    }
                    mSearchedBeers.setText(beerListString);

                } else {
                    mSearchedBeers.setText("");
                }
            }
        });

        setUpMapIfNeeded();

        mMap.setMyLocationEnabled(true);

        //mMap.setOnMyLocationChangeListener(myLocationChangeListener);


        mGoogleApiClient = new GoogleApiClient.Builder(MapsActivity.this)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .build();

        pubList = Pub.listAll(Pub.class);

        markers = new ArrayList<Marker>();

        PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi
                .getCurrentPlace(mGoogleApiClient, null);
        result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
            @Override
            public void onResult(PlaceLikelihoodBuffer likelyPlaces) {
                for (PlaceLikelihood placeLikelihood : likelyPlaces) {
                    Log.i(TAG, String.format("Place '%s' has likelihood: %g",
                            placeLikelihood.getPlace().getName(),
                            placeLikelihood.getLikelihood()));
                    double gapLat = 0;
                    double gapLon = 0;
                    boolean isSame = false;
                    for (int i = 0; i < pubList.size(); i++) {
                        gapLat = Math.abs(placeLikelihood.getPlace().getLatLng().latitude
                                - pubList.get(i).getLatitude());
                        gapLon = Math.abs(placeLikelihood.getPlace().getLatLng().longitude
                                - pubList.get(i).getLongitude());
                        if ((gapLat <0.000001) && (gapLon < 0.000001))
                            isSame = true;
                    }
                    if (!isSame)
                        markers.add(mMap.addMarker(new MarkerOptions()
                                .position(placeLikelihood.getPlace().getLatLng())
                                .title(placeLikelihood.getPlace().getName().toString())
                                .icon(BitmapDescriptorFactory.fromResource(userIcon))
                                .alpha(0.5f)));

                }
                likelyPlaces.release();
            }
        });

        List<Beer> beerList = Beer.listAll(Beer.class);
        String[] beerListString = new String[beerList.size()];
        for (int i = 0; i < beerList.size(); i++)
            beerListString[i] = beerList.get(i).getBeerName();
        ArrayAdapter<String> aBeerOffer = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, beerListString);
        mSearchedBeers.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        mSearchedBeers.setThreshold(1);
        mSearchedBeers.setAdapter(aBeerOffer);

        // Get the string array
        /*String[] countries = getResources().getStringArray(R.array.countries_array);
        System.out.println(countries);*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }


    public void onSearch(View view) {
        mMap.clear();
        for (int i = 0; i < markers.size(); i++) {
            mMap.addMarker (new MarkerOptions()
                    .position(markers.get(i).getPosition())
                    .title(markers.get(i).getTitle())
                    .icon(BitmapDescriptorFactory.fromResource(userIcon))
                    .alpha(0.5f));

        }
        if (pubMarker != null) pubMarker.remove();
        //EditText location_tf = (EditText) findViewById(R.id.multiAutoCompleteSearchedBeers);
        //String location = location_tf.getText().toString();
        String location = mSearchedBeers.getText().toString();
        List<Address> addressList = null;

        if (location != null || !location.equals("")) {

            if (mIsBrewery.isChecked() && mIsNonstop.isChecked())
                Toast.makeText(getBaseContext(),
                        "Brewery cannot be nonstop", Toast.LENGTH_LONG).show();
            else {
                String[] whereArgs = mSearchedBeers.getText().toString().split(", ");
                String whereClause = "";

                if (whereArgs.length > 0) {
                    whereClause = "beer_name = ? ";
                }
                for (int i = 1; i < whereArgs.length; i++) {
                    whereClause += "OR beer_name = ? ";
                }
                LatLng latLng;
                String beerListString = "";

                List<Beer> beerTmp = Beer.find(Beer.class, whereClause, whereArgs);
                String[] beerId = new String[beerTmp.size()];
                for (int i = 0; i < beerTmp.size(); i++)
                    beerId[i] = beerTmp.get(i).getId().toString();

                String whereClause2 = "";
                if (whereArgs.length > 0) {
                    whereClause2 = "beer = ? ";
                }
                for (int i = 1; i < whereArgs.length; i++) {
                    whereClause2 += "OR beer = ? ";
                }
                List<PubHasBeer> tmp = PubHasBeer.findWithQuery(PubHasBeer.class, "SELECT DISTINCT pub FROM pub_has_beer where " + whereClause2, beerId);
                for (int i = 0; i < tmp.size(); i++) {
                    beerListString = "";
                    if ((tmp.get(i).getPub().getIsNonstop() == mIsNonstop.isChecked()) &&
                            (tmp.get(i).getPub().getIsBrevery() == mIsBrewery.isChecked())) {
                        for (int j = 0; j < tmp.get(i).getPub().getBeerOffer().size(); j++) {
                            beerListString += tmp.get(i).getPub().getBeerOffer().get(j).getBeerName();
                            if (j < (tmp.get(i).getPub().getBeerOffer().size() - 1))
                                beerListString += ", ";
                        }
                        latLng = new LatLng((tmp.get(i).getPub().getLatitude()),
                                ((tmp.get(i).getPub().getLongitude())));

                    pubMarker = mMap.addMarker(new MarkerOptions().position(latLng).title(tmp.get(i).getPub().getName())
                            .icon(BitmapDescriptorFactory.fromResource(beerIcon))
                            .snippet("Beers: " + beerListString));
                    }
                    else
                        continue;
                }
            }
        }
        else
            Toast.makeText(getBaseContext(),
                    "You must enter at least one beer ", Toast.LENGTH_LONG).show();
    }

    //mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));


    //MapsDemo
        /*if(location != null || !location.equals(""))
        {
            Geocoder geocoder = new Geocoder(this);
            try {
                addressList = geocoder.getFromLocationName(location , 1);


            } catch (IOException e) {
                e.printStackTrace();
            }
            Address address = addressList.get(0);
            LatLng latLng = new LatLng(address.getLatitude() , address.getLongitude());
            mMap.addMarker(new MarkerOptions().position(latLng).title("Marker"));
            mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

        }*/


    public void changeType(View view) {
        if (mMap.getMapType() == GoogleMap.MAP_TYPE_NORMAL) {
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        } else
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                //setUpMap();

                mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

                    @Override
                    public void onMyLocationChange(Location arg0) {
                        LatLng loc = new LatLng(arg0.getLatitude(), arg0.getLongitude());
                        if (userMarker != null) userMarker.remove();
                        userMarker = mMap.addMarker(new MarkerOptions().position(loc));
                        if (mMap != null) {
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
                        }
                    }
                });
            }
        }
    }

    /*private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
        @Override
        public void onMyLocationChange(Location location) {
            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
            if (userMarker != null) userMarker.remove();
            userMarker = mMap.addMarker(new MarkerOptions().position(loc));
            if (mMap != null) {
                //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
            }
        }
    };*/

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    public void displayAllPubs() {
        LatLng latLng;
        for (int i = 0; i < pubList.size(); i++) {
            latLng = new LatLng((pubList.get(i).getLatitude()),
                    (pubList.get(i).getLongitude()));

            List<Beer> tmp = pubList.get(i).getBeerOffer();
            String beerListString = "";
            for (int j = 0; j < tmp.size(); j++) {
                beerListString += tmp.get(j).getBeerName();
                if (j < (tmp.size()- 1))
                    beerListString += ", ";
            }
            mMap.addMarker(new MarkerOptions().position(latLng).title(pubList.get(i).getName())
                    .icon(BitmapDescriptorFactory.fromResource(beerIcon))
                    .snippet("Beers: " + beerListString));
        }
    }

}
