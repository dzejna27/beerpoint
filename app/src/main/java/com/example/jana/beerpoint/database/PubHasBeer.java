package com.example.jana.beerpoint.database;

import com.orm.SugarRecord;

/**
 * Created by Jana on 22. 11. 2015.
 */
public class PubHasBeer extends SugarRecord<Beer> {
    private Beer beer;
    private Pub pub;

    public PubHasBeer(Beer beer, Pub pub) {
        this.setBeer(beer);
        this.setPub(pub);
    }

    public PubHasBeer(){
    }

    @Override
    public String toString() {
        return "Pub has beer [id= " + id + ", pub= " + getPub() + ", beer= " + getBeer() + "]";
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public Pub getPub() {
        return pub;
    }

    public void setPub(Pub pub) {
        this.pub = pub;
    }
}
