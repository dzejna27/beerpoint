package com.example.jana.beerpoint.database;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Jana on 27. 11. 2015.
 */
public class User extends SugarRecord<User> {
    private String userName;
    private String password;
    @Ignore
    private List<UserRateBeer> userRateBeer;
    @Ignore
    private List<UserRatePub> userRatePub;
    @Ignore
    private List<UserHasBeerList> userHasBeerList;

    public User(){
    }

    public User(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserBeerRating(List<UserRateBeer> userRateBeer) {
        this.userRateBeer = userRateBeer;
    }

    public List<UserRateBeer> getUserBeerRating(){
        return UserRateBeer.find(UserRateBeer.class, "user = ?", this.getId().toString());
    }

    public void setUserPubRating(List<UserRatePub> userRatePub) {
        this.userRatePub = userRatePub;
    }

    public List<UserRatePub> getUserPubRating(){
        return UserRatePub.find(UserRatePub.class, "user = ?", this.getId().toString());
    }

    public void setUserHasBeerList(List<UserHasBeerList> userHasBeerList) {
        this.userHasBeerList = userHasBeerList;
    }

    @Override
    public String toString() {
        return "User [id= " + id + ", user= " + userName + ", pass= " + password + "]";
    }

    public List<Beer> getUserBeerList(){
        ArrayList<UserHasBeerList> tmpUserBeerList = (ArrayList<UserHasBeerList>)
                UserHasBeerList.find(UserHasBeerList.class, "beer = ?", this.getId().toString());
        LinkedList<Beer> tmpBeerList = new LinkedList<>();
        for (UserHasBeerList uhbl : tmpUserBeerList) {
            tmpBeerList.add(uhbl.getBeer());
        }
        return tmpBeerList;
    }
}
