package com.example.jana.beerpoint.database;

import com.orm.SugarRecord;

/**
 * Created by Jana on 6. 11. 2015.
 */

public class Beer extends SugarRecord<Beer>{
    private String beerName;
    private BeerBrand brand;
    private BeerStyle style;
    private float alc;
    private Country country;
    private float ratingTaste;
    private float ratingPrice;
    private Pub pub;

    public Beer(){
    }

    public Beer(String beerName, BeerBrand brand, BeerStyle style, float alc, Country country,
                float ratingTaste, float ratingPrice){
        this.beerName = beerName;
        this.brand = brand;
        this.style = style;
        this.alc = alc;
        this.country = country;
        this.ratingTaste = ratingTaste;
        this.ratingPrice = ratingPrice;
    }

    public Beer(String beerName){
        this.setBeerName(beerName);
    }

    public void setBeerName(String beerName) {
        this.beerName = beerName;
    }

    public String getBeerName() {
        return beerName;
    }

    public void setBrand(BeerBrand brand) {
        this.brand = brand;
    }

    public BeerBrand getBrand() {
        return brand;
    }

    public void setStyle(BeerStyle style) {
        this.style = style;
    }

    public BeerStyle getStyle() {
        return style;
    }

    public void setAlc(float alc) {
        this.alc = alc;
    }

    public float getAlc() {
        return alc;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Country getCountry() {
        return country;
    }

    public void setRatingTaste(float ratingTaste) {
        this.ratingTaste = ratingTaste;
    }

    public float getRatingTaste() {
        return ratingTaste;
    }

    public void setRatingPrice(float ratingPrice) {
        this.ratingPrice = ratingPrice;
    }

    public float getRatingPrice() {
        return ratingPrice;
    }




    @Override
    public String toString() {
        return "Beer [id= " + id + ", name= " + beerName + ", brand= " + brand
                + ", style= " + style + ", alc= " + alc+ ", country= " + country
                + ",  ratingTaste= " + ratingTaste + ",  ratingPrice= " + ratingPrice + "]";
    }

}
