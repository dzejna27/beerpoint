package com.example.jana.beerpoint.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jana.beerpoint.R;
import com.example.jana.beerpoint.database.Beer;
import com.example.jana.beerpoint.database.Pub;
import com.example.jana.beerpoint.database.User;
import com.example.jana.beerpoint.database.UserRateBeer;
import com.example.jana.beerpoint.database.UserRatePub;

import java.util.List;

public class RateActivity extends AppCompatActivity {

    private AutoCompleteTextView mBeerName;
    private RatingBar mTaste;
    private RatingBar mBeerPrice;

    private AutoCompleteTextView mPubName;
    private RatingBar mAvailability;
    private RatingBar mAtmosphere;
    private RatingBar mService;
    private RatingBar mQuality;
    private RatingBar mPubPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle parameters = getIntent().getExtras();
        if (parameters != null) {
            if (parameters.containsKey("beer")) {
                setContentView(parameters.getInt("beer"));
                {
                    mBeerName = (AutoCompleteTextView) findViewById(R.id.autoCompleteRateBeerName);
                    mTaste = (RatingBar) findViewById(R.id.ratingBarTaste);
                    mBeerPrice = (RatingBar) findViewById(R.id.ratingBarBeerPrice);

                    List<Beer> beerList = Beer.listAll(Beer.class);
                    String[] beerListString = new String[beerList.size()];
                    System.out.println("rate: "+ beerList);
                    for (int i = 0; i < beerList.size(); i++)
                        System.out.println(i+ "rateOffer: "+ (beerListString[i] = beerList.get(i).getBeerName()));
                    ArrayAdapter<String> aBeerOffer = new ArrayAdapter<String>(this,
                            android.R.layout.simple_list_item_1, beerListString);
                    mBeerName.setAdapter(aBeerOffer);
                                    }


            }
            if (parameters.containsKey("pub")) {
                setContentView(parameters.getInt("pub"));
                {
                    mPubName = (AutoCompleteTextView) findViewById(R.id.autoCompleteRatePubName);
                    mAtmosphere = (RatingBar) findViewById(R.id.ratingBarAtmosphere);
                    mService = (RatingBar) findViewById(R.id.ratingBarService);
                    mAvailability = (RatingBar) findViewById(R.id.ratingBarAvailability);
                    mQuality = (RatingBar) findViewById(R.id.ratingBarQuality);
                    mPubPrice = (RatingBar) findViewById(R.id.ratingBarPubPrice);

                    List<Pub> pubList = Pub.listAll(Pub.class);
                    String[] pubListString = new String[pubList.size()];
                    System.out.println("rate: "+ pubList);
                    for (int i = 0; i < pubList.size(); i++)
                        System.out.println(i+ "rateOffer: "+ (pubListString[i] = pubList.get(i).getName()));
                    ArrayAdapter<String> aPubOffer = new ArrayAdapter<String>(this,
                            android.R.layout.simple_list_item_1, pubListString);
                    mPubName.setAdapter(aPubOffer);
                }
            }

        }
    }

    public void clkBeerRate(View view) {
        final List<Beer> beerTmp = Beer.find(Beer.class, "beer_name = ?", mBeerName.getText().toString());
        Beer ratedBeer = Beer.findById(Beer.class, beerTmp.get(0).getId());
        final List<User> userTmp = User.find(User.class, "user_name = ?", "user123");
        UserRateBeer userRateBeer = new UserRateBeer(userTmp.get(0), ratedBeer,
                mTaste.getRating(), mBeerPrice.getRating());
        userRateBeer.save();

        final List<UserRateBeer> ratingsOfBeer = UserRateBeer.findWithQuery(UserRateBeer.class,
                "SELECT beer FROM user_rate_beer where beer = "+beerTmp.get(0).getId());
        int ratingsCount = ratingsOfBeer.size();
        System.out.println("pocet: " + ratingsCount);
        if (ratingsCount > 0) {
            ratedBeer.setRatingTaste(((ratedBeer.getRatingTaste() * (ratingsCount - 1))
                    + mTaste.getRating()) / (float) (ratingsCount));
            ratedBeer.setRatingPrice(((ratedBeer.getRatingPrice() * (ratingsCount - 1))
                    + mBeerPrice.getRating()) / (float) (ratingsCount));
            ratedBeer.save();
            Toast.makeText(getBaseContext(),
                    "Rating added", Toast.LENGTH_LONG).show();
            Intent iBPBMenu = new Intent(this, BPBMenuActivity.class);
            iBPBMenu.putExtra("beer", R.layout.activity_bpb_menu);
            startActivity(iBPBMenu);
        }
    }

    public void clkPubRate(View view) {
        final List<Pub> pubTmp = Pub.find(Pub.class, "name = ?", mPubName.getText().toString());
        Pub ratedPub = Pub.findById(Pub.class, pubTmp.get(0).getId());
        final List<User> userTmp = User.find(User.class, "user_name = ?", "user123");
        UserRatePub userRatePub = new UserRatePub(userTmp.get(0), ratedPub,
                                mAtmosphere.getRating(), mService.getRating(),
                                mAvailability.getRating(), mQuality.getRating(),
                                mPubPrice.getRating());
        userRatePub.save();

        final List<UserRatePub> ratingsOfPub = UserRatePub.findWithQuery(UserRatePub.class,
                "SELECT pub FROM user_rate_pub where pub = " + pubTmp.get(0).getId());
        int ratingsCount = ratingsOfPub.size();
        if (ratingsCount > 0) {
            ratedPub.setAtmosphere(((ratedPub.getAtmosphere() * (ratingsCount - 1))
                    + mAtmosphere.getRating()) / (float) (ratingsCount));
            ratedPub.setService(((ratedPub.getService() * (ratingsCount - 1))
                    + mService.getRating()) / (float) (ratingsCount));
            ratedPub.setAvailability(((ratedPub.getAvailability() * (ratingsCount - 1))
                    + mAvailability.getRating()) / (float) (ratingsCount));
            ratedPub.setQuality(((ratedPub.getQuality() * (ratingsCount - 1))
                    + mQuality.getRating()) / (float) (ratingsCount));
            ratedPub.setPrice(((ratedPub.getPrice() * (ratingsCount - 1))
                    + mPubPrice.getRating()) / (float) (ratingsCount));
            ratedPub.save();
            Toast.makeText(getBaseContext(),
                    "Rating added", Toast.LENGTH_LONG).show();
            Intent iBPBMenu = new Intent(this, BPBMenuActivity.class);
            iBPBMenu.putExtra("pub", R.layout.activity_bpb_menu);
            startActivity(iBPBMenu);
        }
    }
}
