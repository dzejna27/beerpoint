package com.example.jana.beerpoint.database;

import com.orm.SugarRecord;

/**
 * Created by Jana on 28. 11. 2015.
 */

public class UserRateBeer extends SugarRecord<UserRateBeer> {
    private User user;
    private Beer beer;
    private float taste;
    private float beerPrice;

    public UserRateBeer() {
    }

    public UserRateBeer(User user, Beer beer, float taste, float beerPrice) {
        this.setUser(user);
        this.setBeer(beer);
        this.taste = taste;
        this.beerPrice = beerPrice;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public float getTaste() {
        return taste;
    }

    public void setTaste(float taste) {
        this.taste = taste;
    }

    public float getBeerPrice() {
        return beerPrice;
    }

    public void setBeerPrice(float beerPrice) {
        this.beerPrice = beerPrice;
    }

    @Override
    public String toString() {
        return "User rate beer [id= " + id + ", user= " + getUser() + ", beer= " + getBeer() +
                ",  ratingTaste= " + taste + ",  ratingPrice= " + beerPrice + "]";
    }
}