package com.example.jana.beerpoint.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.jana.beerpoint.R;
import com.example.jana.beerpoint.database.Beer;
import com.example.jana.beerpoint.database.BeerBrand;
import com.example.jana.beerpoint.database.Country;
import com.example.jana.beerpoint.database.PubHasBeer;
import com.example.jana.beerpoint.database.BeerStyle;
import com.example.jana.beerpoint.database.Pub;
import com.example.jana.beerpoint.database.User;
import com.example.jana.beerpoint.database.UserHasBeerList;
import com.example.jana.beerpoint.database.UserRateBeer;
import com.example.jana.beerpoint.database.UserRatePub;
import com.example.jana.beerpoint.support.AlertDialogManager;
import com.example.jana.beerpoint.support.ConnectionDetector;
import com.example.jana.beerpoint.support.GPSTracker;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public Random rn = new Random();

    private Beer beer;
    private BeerBrand beerBrand;
    private BeerStyle beerStyle;
    private Country country;
    private User user;
    private UserHasBeerList userBeerList;
//    static MainActivity INSTANCE;
//    String data="FirstActivity";

//    private boolean consumedIntent;

    // flag for Internet connection status
    Boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;

    // Alert Dialog Manager
    AlertDialogManager alert = new AlertDialogManager();

    // GPS Location
    GPSTracker gps;

    // Button
    Button btnShowOnMap;



    // KEY Strings
    public static String KEY_REFERENCE = "reference"; // id of the place
    public static String KEY_NAME = "name"; // name of the place
    public static String KEY_VICINITY = "vicinity"; // Place area name
    private static final int PLACE_PICKER_REQUEST = 1000;
    private GoogleApiClient mClient;

    private final String SAVED_INSTANCE_STATE_CONSUMED_INTENT = "SAVED_INSTANCE_STATE_CONSUMED_INTENT";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        INSTANCE=this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        initDBs();
//        Pub.deleteAll(Pub.class);
//        PubHasBeer.deleteAll(PubHasBeer.class);

//        if( savedInstanceState != null ) {
//            consumedIntent = savedInstanceState.getBoolean(SAVED_INSTANCE_STATE_CONSUMED_INTENT);
//        }

        cd = new ConnectionDetector(getApplicationContext());
        

        // Check if Internet present
        isInternetPresent = cd.isConnectingToInternet();
        if (!isInternetPresent) {
            // Internet Connection is not present
            alert.showAlertDialog(MainActivity.this, "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }

        // creating GPS Class object
        gps = new GPSTracker(this);

        // check if GPS location can get
        if (gps.canGetLocation()) {
            Log.d("Your Location", "latitude:" + gps.getLatitude() + ", longitude: " + gps.getLongitude());
        } else {
            // Can't get user's current location
            alert.showAlertDialog(MainActivity.this, "GPS Status",
                    "Couldn't get location information. Please enable GPS",
                    false);
            // stop executing code by return
            return;
        }

        Button button1 = (Button) findViewById(R.id.bBeer);
        button1.setOnClickListener(this);

        Button button2 = (Button) findViewById(R.id.bPub);
        button2.setOnClickListener(this);

    }

    //    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        outState.putBoolean(SAVED_INSTANCE_STATE_CONSUMED_INTENT, consumedIntent);
//    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public void onResume() {
        super.onResume();

        Intent theIntent = getIntent();
        if (theIntent.getExtras() != null) {
            onNewIntent(new Intent());  // <--- "clear" the intent by setting empty one
        }
    }

    @Override
        public void onClick(View view) {

        listAll();

        if (this.getIntent().getExtras() != null) {
            this.getIntent().replaceExtras(new Intent());
            onNewIntent(new Intent());
            getIntent().setAction("");
            getIntent().setData(null);
            getIntent().setFlags(0);}

            //getIntent().replaceExtras(new Intent());
        Intent iBPBMenu = new Intent(this, BPBMenuActivity.class);
        switch (view.getId()) {
            case R.id.bBeer:
                iBPBMenu.putExtra("beer", R.layout.activity_bpb_menu);
                break;
            case R.id.bPub:
                iBPBMenu.putExtra("pub", R.layout.activity_bpb_menu);
                break;
        }
        startActivity(iBPBMenu);
    }

    public void initDBs() {
        Beer.deleteAll(Beer.class);
        BeerBrand.deleteAll(BeerBrand.class);
        BeerStyle.deleteAll(BeerStyle.class);
        Country.deleteAll(Country.class);
        Pub.deleteAll(Pub.class);
        PubHasBeer.deleteAll(PubHasBeer.class);
        User.deleteAll(User.class);
        UserHasBeerList.deleteAll(UserHasBeerList.class);
        UserRateBeer.deleteAll(UserRateBeer.class);
        UserRatePub.deleteAll(UserRatePub.class);

        String[] brands = getResources().getStringArray(R.array.beer_brands);
        for (int i = 0; i < brands.length; i++) {
            System.out.println(beerBrand = new BeerBrand(brands[i]));
            beerBrand.save();
        }

        String[] styles = getResources().getStringArray(R.array.beer_styles);
        for (int i = 0; i < styles.length; i++) {
                    System.out.println(beerStyle = new BeerStyle(styles[i]));
                    beerStyle.save();
        }

        String[] countries = getResources().getStringArray(R.array.countries_array);
        for (int i = 0; i < countries.length; i++) {
            System.out.println(country = new Country(countries[i]));
            country.save();
        }

        beer = new Beer("Kamzik",
                BeerBrand.find(BeerBrand.class,
                        "brand = ?", "Chimay").get(0),
                BeerStyle.find(BeerStyle.class,
                        "style = ?", "STOUT").get(0), (float) 4.0,
                Country.find(Country.class,
                        "country = ?", "Fiji").get(0), (float) (5.0), (float) (5.0));
        beer.save();

        beer = new Beer("Corgon",
                BeerBrand.find(BeerBrand.class,
                        "brand = ?", "Skol").get(0),
                BeerStyle.find(BeerStyle.class,
                        "style = ?", "PORTER").get(0), (float) 5.0,
                Country.find(Country.class,
                        "country = ?", "Finland").get(0),
                (float) (4.0), (float) (5.0));
        beer.save();

        beer = new Beer("Popper",
                BeerBrand.find(BeerBrand.class,
                        "brand = ?", "Hinano").get(0),
                BeerStyle.find(BeerStyle.class,
                        "style = ?", "PERRY").get(0), (float) 2.0,
                Country.find(Country.class,
                        "country = ?", "Russia").get(0),
                (float) (1.0), (float) (2.0));
        beer.save();

        beer = new Beer("Saris",
                BeerBrand.find(BeerBrand.class,
                        "brand = ?", "Snow Beer").get(0),
                BeerStyle.find(BeerStyle.class,
                        "style = ?", "DARK LAGER").get(0), (float) 2.0,
                Country.find(Country.class,
                        "country = ?", "Slovakia").get(0),
                (float) (2.0), (float) (3.5));
        beer.save();

        beer = new Beer("Karjala",
                BeerBrand.find(BeerBrand.class,
                        "brand = ?", "Heineken").get(0),
                BeerStyle.find(BeerStyle.class,
                        "style = ?", "LIGHT LAGER").get(0), (float) 4.0,
                Country.find(Country.class,
                        "country = ?", "Finland").get(0)
                , (float) (5.0), (float) (5.0));
        beer.save();

        beer = new Beer("Karhu",
                BeerBrand.find(BeerBrand.class,
                        "brand = ?", "Heineken").get(0),
                BeerStyle.find(BeerStyle.class,
                        "style = ?", "LIGHT LAGER").get(0), (float) 5.0,
                Country.find(Country.class,
                        "country = ?", "Finland").get(0),
                (float) (4.0), (float) (5.0));
        beer.save();

        beer = new Beer("Ziguli",
                BeerBrand.find(BeerBrand.class,
                        "brand = ?", "Baltika").get(0),
                BeerStyle.find(BeerStyle.class,
                        "style = ?", "LIGHT LAGER").get(0), (float) 2.0,
                Country.find(Country.class,
                        "country = ?", "Russia").get(0),
                (float) (1.0), (float) (2.0));
        beer.save();


        user = new User("user123", "123");

        user.save();

        final List<User> userTmp = User.find(User.class, "user_name = ?", "user123");

        String[] whereArgs = new String[2];
        whereArgs[0] = "Popper";
        whereArgs[1] = "Corgon";
        String whereClause = "";
        if(whereArgs.length > 0){
            whereClause = "beer_name = ? ";
        }
        for(int i=1 ;i<whereArgs.length; i++){
            whereClause += "OR beer_name = ? ";
        }
        List<Beer> beersInList = Beer.find(Beer.class, whereClause, whereArgs);
        userBeerList = new UserHasBeerList(userTmp.get(0), beersInList.get(0));
        userBeerList.save();
        userBeerList = new UserHasBeerList(userTmp.get(0), beersInList.get(1));
        userBeerList.save();
    }

    public void listAll() {
        System.out.println("zobrazPivo:" + Beer.listAll(Beer.class));
        System.out.println("zobrazZnacku:" + BeerBrand.listAll(BeerBrand.class));
        System.out.println("zobrazStyl:" + BeerStyle.listAll(BeerStyle.class));
        System.out.println("zobrazCountries:" + Country.listAll(Country.class));
        System.out.println("zobrazPubs:" + Pub.listAll(Pub.class));
        if(Pub.listAll(Pub.class).size()>0)
            System.out.println("zobrazBeerOffer:" + Pub.listAll(Pub.class).get(0).getBeerOffer());
        System.out.println("zobrazBP:" + PubHasBeer.listAll(PubHasBeer.class));
        System.out.println("zobrazUser:" + User.listAll(User.class));
        System.out.println("zobrazURB:" + UserRateBeer.listAll(UserRateBeer.class));
        System.out.println("zobrazURP:" + UserRatePub.listAll(UserRatePub.class));
        System.out.println("zobrazUHBL:" + UserHasBeerList.listAll(UserHasBeerList.class));
    }

}