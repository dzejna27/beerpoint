package com.example.jana.beerpoint.database;

import com.orm.SugarRecord;

/**
 * Created by Jana on 28. 11. 2015.
 */
public class UserHasBeerList extends SugarRecord<UserRateBeer> {
    private User user;
    private Beer beer;

    public UserHasBeerList (){
    }

    public UserHasBeerList(User user, Beer beer) {
        this.setUser(user);
        this.setBeer(beer);
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    @Override
    public String toString() {
        return "User has beer [id= " + id + ", user= " + getUser() + ", beer= " + getBeer() + "]";
    }
}
