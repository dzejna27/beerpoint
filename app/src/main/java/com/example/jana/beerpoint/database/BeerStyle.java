package com.example.jana.beerpoint.database;

import com.example.jana.beerpoint.database.Beer;
import com.orm.SugarRecord;

/**
 * Created by Jana on 17. 11. 2015.
 */
public class BeerStyle extends SugarRecord<Beer> {
    private String style;

    public BeerStyle(){
    }

    public BeerStyle(String style) {
        this.setBeerStyle(style);
    }

    public void setBeerStyle(String style) {
        this.style = style;
    }

    public String getBeerStyle() {
        return style;
    }

    @Override
    public String toString() {
        return "Beer style [id= " + id + " style= " + style + " ]";
    }

//    @Override
//    public String toString() {
//        return style;
//    }
}
